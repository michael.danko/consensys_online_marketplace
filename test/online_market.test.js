var Administrator = artifacts.require('Administrator')

/* All function tests were written to unit test individual functions to ensure any changes in the future will not break the application */

contract('Administrator', function(accounts) {

    const owner = accounts[0]
    const alice = accounts[1]
    const bob = accounts[2]
    const emptyAddress = '0x0000000000000000000000000000000000000000'
    const testAddress = '0x0000000000000000000000000000000000000123'

    var sku
    const price = web3.toWei(1, "ether")

    it("should add an item with the provided name and price", async() => {
        const contract = await Administrator.deployed()

        var eventEmitted = false

        var event = contract.ForSale()
        await event.watch((err, res) => {
            sku = res.args.sku.toString(10)
            eventEmitted = true
        })

        const store = 0
        const putSku = 0
        const name = "book"
        const quantity = 1
        const price = 1

        await contract.putItem(store, putSku, name, quantity, price, {from: alice})

        const result = await contract.getItem.call(store, sku)

        assert.equal(result[0], name, 'the name of the last added item does not match the expected value')
        assert.equal(result[1], quantity, 'the quantity of the last added item does not match the expected value')
        assert.equal(result[2].toString(10), price, 'the price of the last added item does not match the expected value')
        assert.equal(eventEmitted, true, 'adding an item should emit a For Sale event')
    })

    it("should allow someone to change a store name", async() => {
        const contract = await Administrator.deployed()

        var eventEmitted = false

        var event = contract.StoreChangedName()
        await event.watch((err, res) => {
            store = res.args.store.toString(10)
            eventEmitted = true
        })

        var storeId = 0 
        var storeName = "Test Store"
        
        await contract.changeStoreName(storeId, storeName, {from:alice})

        const result = await contract.getStoreName.call(storeId)

        assert.equal(result, storeName, 'The store name should match')
    })

    it("should allow the administrator to assign store owners", async() => {
        const contract = await Administrator.deployed()

        var eventEmitted = false

        var event = contract.NewStoreOwnerAssigned()
        await event.watch((err, res) => {
            _address = res.args._address
            eventEmitted = true
        })

        await contract.addAdministrator(testAddress, {from: alice})

        const result = await contract.getAdministratorAccess.call(testAddress)

        assert.equal(eventEmitted, true, 'Adding Store Owners should emit an event')
        assert.equal(result, true, 'Administrator has been added to the system')
    })


    it("should allow the administrator to change store owners", async() => {
        const contract = await Administrator.deployed()

        var eventEmitted = false

        var event = contract.StoreAdminChanged()
        await event.watch((err, res) => {
            address = res.args.address
            eventEmitted = true
        })

        await contract.changeStoreAdmin(0, testAddress, {from: alice})

        const result = await contract.getStoreAdmin.call(0)

        assert.equal(eventEmitted, true, 'Removing Store Owners should emit an event')
        assert.equal(result, testAddress, 'Store Owner has been changed to the correct address')
    })

    it("should allow the administrator to remove store owners", async() => {
        const contract = await Administrator.deployed()

        var eventEmitted = false

        var event = contract.StoreOwnerRemoved()
        await event.watch((err, res) => {
            address = res.args.address
            eventEmitted = true
        })

        await contract.removeAdministrator(testAddress, {from: alice})

        const result = await contract.getAdministratorAccess.call(testAddress)

        assert.equal(eventEmitted, true, 'Removing Store Owners should emit an event')
        assert.equal(result, false, 'Administrator access should have been revoked')
    })


});
