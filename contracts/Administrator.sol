pragma solidity ^0.4.23;

contract Administrator {

  /* set owner */
  address owner;
  address[16] public administratorsArray;

  struct Item {
    string name;
    uint sku;
    uint quantity;
    uint price;
  }
  
  struct Store {
    string name;
    address administrator;
    Item[12] itemArray;
  }

  Store[4] public storeArray;

  /* Map of all the administrative users */
  mapping (address => bool) public administrators;
	mapping (address => uint) private storeOwnerBalances;

  constructor() public {
    /* Set the owner as the person who instantiated the contract */
    owner = msg.sender;
    administrators[msg.sender] = true;
  }

  event ForSale(uint sku);
  event StoreChangedName(uint store);
  event NewStoreOwnerAssigned(address storeOwner);
  event StoreOwnerRemoved(address storeOwner);
  event StoreAdminChanged(address newAdministrator);

  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

   modifier administrator() {
    require(administrators[msg.sender] = true);
    _;
  } 

  /* Add administrator the the system */
  function addAdministrator (address _addAdministrator) public returns (bool) {
    /* only administrators can add other administrators */
    require(administrators[msg.sender] = true);
    administrators[_addAdministrator] = true;
    emit NewStoreOwnerAssigned(_addAdministrator);
    return true;
  }

  function removeAdministrator (address _removeAdministrator) public returns (bool) {
    require(_removeAdministrator != owner);
    administrators[_removeAdministrator] = false;
    emit StoreOwnerRemoved(_removeAdministrator);
    return true;
  }

  function getAdministratorAccess(address checkAdministrator) public view returns (bool) {
    return administrators[checkAdministrator];
  }

  function getStoreAdmin(uint store) public view returns (address) {
    return storeArray[store].administrator;
  }

  function getStoreName(uint store) public view returns (string) {
    return storeArray[store].name;
  }

  function changeStoreAdmin (uint store, address newAdministrator) public returns (bool) {
    emit StoreAdminChanged(newAdministrator);
    storeArray[store].administrator = newAdministrator;
  }

  function changeStoreName (uint store, string newName) public returns (bool) {
    emit StoreChangedName(store);
    storeArray[store].name = newName;
  }

  function getStoreOwnerAccess(address checkStoreOwner) public view returns (bool, uint) {
    for(uint i=0;i<4;i++) {
      if(storeArray[i].administrator == checkStoreOwner) {
        return(true, i);
      }
      else if(getAdministratorAccess(checkStoreOwner) == true) {
				return(true, 0);
			}
        
       
    }
    return(false,0);
  }

  function getItem(uint store, uint sku) public view returns (string, uint, uint) {
    string storage name = storeArray[store].itemArray[sku].name;
    uint quantity = storeArray[store].itemArray[sku].quantity;
    uint price = storeArray[store].itemArray[sku].price;
    return(name, quantity, price);
  }

  function putItem(uint store, uint sku, string name, uint quantity, uint price) public returns (bool) {
    storeArray[store].itemArray[sku].name = name;
    storeArray[store].itemArray[sku].quantity = quantity;
    storeArray[store].itemArray[sku].price = price;
		emit ForSale(sku);
  }

  function buyItem(uint store, uint sku, uint quantity) public payable returns (bool) {
    require(msg.value >= quantity * storeArray[store].itemArray[sku].price && storeArray[store].itemArray[sku].price + msg.value >= storeArray[store].itemArray[sku].price);
    /* check for overflow or underflow */	
		if(msg.value < quantity * storeArray[store].itemArray[sku].price) {
      return false;
		} else if (quantity < storeArray[store].itemArray[sku].quantity) {
			return false;
		} else if (msg.value >= quantity * storeArray[store].itemArray[sku].price) {
			storeArray[store].itemArray[sku].quantity -= quantity;
			storeOwnerBalances[storeArray[store].administrator] += msg.value;   		   
		}
}

/* avoid re-entrancy by completing internal work before making external calls */
function withdrawBalance() public {
    uint amountToWithdraw = storeOwnerBalances[msg.sender];
	  storeOwnerBalances[msg.sender] = 0;
		require(msg.sender.call.value(amountToWithdraw)());   }
}
