##  Consensys Academy Final Project
-----------------------------------

For the final project I attempted the marketplace project. In order to setup this project locally you must complete the following steps:

git clone https://gitlab.com/michael.danko/consensys_online_marketplace.git

npm install truffle

npm install ganache-cli

start ganache-cli, save the 12 word mnemonic to your Meta Mask

you can then navigate into the consensys_online_marketplace folder and truffle compile, truffle test, truffle migrate, this will put the contracts on the ganache-cli blockchain

finally start npm run dev from the truffle directory to start a web server

you can use the dapp as you please.

