App = {
  web3Provider: null,
  contracts: {},

  init: function() {
		var productsRow = $('#productsRow');
    var productTemplate = $('#productTemplate');
    
    for (i = 0; i < 4; i++) {
      productTemplate.children().addClass('store-' + i);
			productTemplate.children().attr('store-number',  i);
      for (j = 0; j < 4; j++) {
        productTemplate.find('.panel-title').text("sku " + j); 
				productTemplate.children().attr("sku", j);

		  	productsRow.append(productTemplate.html());
      }
	    productTemplate.children().removeClass('store-' + i);	
		}

    // Load pets.
//    $.getJSON('../pets.json', function(data) {
//      var petsRow = $('#petsRow');
//      var petTemplate = $('#petTemplate');
//
//      for (i = 0; i < data.length; i ++) {
//        petTemplate.find('.panel-title').text(data[i].name);
//        petTemplate.find('img').attr('src', data[i].picture);
//        petTemplate.find('.pet-breed').text(data[i].breed);
//        petTemplate.find('.pet-age').text(data[i].age);
//        petTemplate.find('.pet-location').text(data[i].location);
//        petTemplate.find('.btn-adopt').attr('data-id', data[i].id);
//
//        petsRow.append(petTemplate.html());
//      }
//    });

    return App.initWeb3();
  },

  initWeb3: function() {
    if(typeof web3 !== 'undefined'){
      App.web3Provider = web3.currentProvider
    } else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545')
    }

    web3 = new Web3(App.web3Provider)
      return App.initContract();
  },

  initContract: function() {
    $.getJSON('Administrator.json', function(data){
      var AdministratorArtifact = data
      App.contracts.Administrator = TruffleContract(AdministratorArtifact)
      App.contracts.Administrator.setProvider(App.web3Provider)
      return App.markAdministrators()
    });

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-adopt', App.handleAdopt);
    $(document).on('click', '.btn-add', App.addAdministrator);
    $(document).on('click', '.btn-remove', App.removeAdministrator);
    $(document).on('click', '#chng-admin', App.changeStoreOwner);
    $(document).on('click', '#chng-name', App.changeStoreName);
    $(document).on('click', '.btn-chng-prod', App.putItem);
    $(document).on('click', '.btn-buy-prod', App.buyItem);

  },

  markAdministrators: function(administrators, account) {
    /* App.contracts.Administrator.deployed().then(function(instance){
      administratorsInstance = instance
      return administratorsInstance.getAdministratorAccess.call(web3.eth.accounts)
    }).then(function(administrators){
      if (administrators == true) {
         $('#template').css("display", "block")
      } 
      console.log(administrators)
      for(var i = 0; i < administrators.length; i++) {
        console.log(administrators[i])
      }
    }).then(App.checkStoreOwner).catch(function(err){
      console.log(err.message)
    })*/

    App.contracts.Administrator.deployed().then(function(instance){
      administratorsInstance = instance
      return administratorsInstance.getStoreOwnerAccess.call(web3.eth.accounts)
    }).then(function(returnedValues){
      console.log(returnedValues)
      if (returnedValues[0] == true) {
         $('.store-owner').css("display", "block")
      } 
    }).catch(function(err){
      console.log(err.message)
    })

    /* retrieve store admin address to display */
    App.contracts.Administrator.deployed().then(function(instance){
       administratorsInstance = instance
        administratorsInstance.getStoreAdmin.call(0).then(function(result){
          console.log("Owner Address:" + result)
          $("div#owner-address-0").text(result)
        })
      }).catch(function(err){
      console.log(err.message)
    })
 
    App.contracts.Administrator.deployed().then(function(instance){
       administratorsInstance = instance
        administratorsInstance.getStoreName.call(0).then(function(result){
          if (result === "")
            result = "Store 0"
          console.log("Store Name:" + result)
          $("#store-name-0").text(result)
          $("#tablink-0").text(result)
        })
      }).catch(function(err){
      console.log(err.message)
    })
    
    App.contracts.Administrator.deployed().then(function(instance){
       administratorsInstance = instance
        administratorsInstance.getItem.call(0,0).then(function(result){
					console.log(result)
          if (result === "")
            result = "Store 0"
        })
      }).catch(function(err){
      console.log(err.message)
    })

    App.contracts.Administrator.deployed().then(function(instance){
       administratorsInstance = instance
        administratorsInstance.getItem.call(0,0).then(function(result){
					$('[sku="0"]').find('input.name').val(result[0])
					$('[sku="0"]').find('input.quantity').val(result[1])
					$('[sku="0"]').find('input.price').val(result[2])
      	})
				administratorsInstance.getItem.call(0,1).then(function(result){
					$('[sku="1"]').find('input.name').val(result[0])
					$('[sku="1"]').find('input.quantity').val(result[1])
					$('[sku="1"]').find('input.price').val(result[2])
      	})
				administratorsInstance.getItem.call(0,2).then(function(result){
					$('[sku="2"]').find('input.name').val(result[0])
					$('[sku="2"]').find('input.quantity').val(result[1])
					$('[sku="2"]').find('input.price').val(result[2])
      	})
				administratorsInstance.getItem.call(0,3).then(function(result){
					$('[sku="3"]').find('input.name').val(result[0])
					$('[sku="3"]').find('input.quantity').val(result[1])
					$('[sku="3"]').find('input.price').val(result[2])
      	})

      }).catch(function(err){
      console.log(err.message)
    })

  },
  
  markAdopted: function(adopters, account) {
    var adoptionInstance 
    App.contracts.Adoption.deployed().then(function(instance){
      adoptionInstance = instance
      return adoptionInstance.getAdopters.call()
    }).then(function(adopters){
      for(var i = 0; i < adopters.length; i++) {
        if (adopters[i] !== '0x0000000000000000000000000000000'){
          $('.panel-pet').eq(i).find('button').text('Success').attr('disabled', true)
        }
      }
    }).catch(function(err){
      console.log(err.message)
    })
    /*
     * Replace me...
     */
  },

  addAdministrator: function(event) {

    var administratorInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
    
      var account = accounts[0];
    
      App.contracts.Administrator.deployed().then(function(instance) {
        administratorInstance = instance;
        administratorId = $('#admin-add').val()
        // Execute adopt as a transaction by sending account
        return administratorInstance.addAdministrator(administratorId, {from: account});
      }).then(function(result) {
        return App.markAdministrators();
      }).catch(function(err) {
        console.log(err.message);
      });
    });
  },

  removeAdministrator: function(event) {

    var administratorInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
    
      var account = accounts[0];
    
      App.contracts.Administrator.deployed().then(function(instance) {
        administratorInstance = instance;
        administratorId = $('#admin-remove').val()
        // Execute adopt as a transaction by sending account
        return administratorInstance.removeAdministrator(administratorId, {from: account});
      }).then(function(result) {
        return App.markAdministrators();
      }).catch(function(err) {
        console.log(err.message);
      });
    });
  },

  changeStoreOwner: function(event) {
    var administratorInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
    
      var account = accounts[0];
    
      App.contracts.Administrator.deployed().then(function(instance) {
        administratorInstance = instance;
        administratorId = $('#store-change-admin').val()
        console.log(administratorId)
        storeId = $('#store-change-admin').attr('store-number')
        console.log(storeId)
        // Execute adopt as a transaction by sending account
        console.log(storeId + administratorId)
        return administratorInstance.changeStoreAdmin(storeId, administratorId, {from: account});
      }).then(function(result) {
        return App.markAdministrators();
      }).catch(function(err) {
        console.log(err.message);
      });
    }); 
  },

  changeStoreName: function(event) {
    var administratorInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
    
      var account = accounts[0];
    
      App.contracts.Administrator.deployed().then(function(instance) {
        administratorInstance = instance;
        storeName = $('#store-change-name').val()
        console.log(storeName)
        storeId = $('#store-change-name').attr('store-number')
        console.log(storeId)
        // Execute adopt as a transaction by sending account
        console.log(storeId + storeName)
        return administratorInstance.changeStoreName(storeId, storeName, {from: account});
      }).then(function(result) {
        return App.markAdministrators();
      }).catch(function(err) {
        console.log(err.message);
      });
    }); 
  },

  putItem: function(event) {
    var administratorinstance;
 
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
    
      var account = accounts[0];
    
      App.contracts.Administrator.deployed().then(function(instance) {
        administratorInstance = instance;

        storeNumber = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.attributes[2].value
			  productSku = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.attributes[3].value

				productName = $('[sku="' + productSku +'"]').find('.name').val()
				productQuantity = $('[sku="' + productSku +'"]').find('.quantity').val()
  		  productPrice = $('[sku="' + productSku +'"]').find('.price').val()
				console.log(storeNumber, productSku, productName, productQuantity, productPrice)
        return administratorInstance.putItem(storeNumber, productSku, productName, productQuantity, productPrice, {from: account});
      }).then(function(result) {
        return App.markAdministrators();
      }).catch(function(err) {
        console.log(err);
      });
    }); 
  },

  buyItem: function(event) {
    var administratorinstance;
 
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }
    
      var account = accounts[0];
    
      App.contracts.Administrator.deployed().then(function(instance) {
        administratorInstance = instance;

        storeNumber = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.attributes[2].value
			  productSku = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.attributes[3].value

				//productName = $('[sku="' + productSku +'"]').find('.name').val()
				productQuantity = $('[sku="' + productSku +'"]').find('.quantity').val()
  		  productPrice = $('[sku="' + productSku +'"]').find('.price').val()
        price = productQuantity * productPrice 
				console.log(storeNumber, productSku, productQuantity)
        return administratorInstance.buyItem(storeNumber, productSku, productQuantity, {value: price, from: account});
      }).then(function(result) {
        return App.markAdministrators();
      }).catch(function(err) {
        console.log(err);
      });
    }); 
  },

  handleAdopt: function(event) {
    event.preventDefault();

    var petId = parseInt($(event.target).data('id'));

    /*
     * Replace me...
     */
  }

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
